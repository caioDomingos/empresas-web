import React from 'react';

import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom';
import Details from './views/Details/Details';
import Home from './views/Home/Home';
import Login from './views/Login/Login';
import NotFound from './views/NotFound/NotFound';

export default (_) => {
  return (
    <Router>
        <Switch>
          <Route path='/login'>
            <Login></Login>
          </Route>
          <Route path='/details/:id'>
            <Details></Details>
          </Route>
          <Route exact path='/'>
            <Home></Home>
          </Route>
          <Route path='*'>
            <NotFound></NotFound>
          </Route>
        </Switch>
    </Router>
  );
};
