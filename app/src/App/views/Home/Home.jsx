import React, { useState } from 'react';
import { useHistory, Redirect, useLocation } from 'react-router-dom';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import Loader from 'react-loader-spinner';
import 'react-toastify/dist/ReactToastify.min.css';
import { toast, ToastContainer } from 'react-toastify';

import './Home.css';

import Header from './../../components/header/Header';
import Card from './../../components/Card/Card';
import { getByName } from '../../providers/enterprises';

export default (_) => {
  const [list, setList] = useState([]);
  const [input, setInput] = useState('');

  const [handleLoading, setHandleLoading] = useState(false);

  const history = useHistory();
  const location = useLocation();

  const handleChangeSearchbarInput = async (input) => {
    try {
      console.log(`Searchbar change in Home`, input);
      setInput(input);
      if (!input) {
        setList([]);
        return;
      }
      setHandleLoading(true);
      const response = await getByName(input);
      var arrLength = response.length;
      let listResponse = response;
      if (arrLength > 10) {
        listResponse = response.splice(0, 10);
      }
      setList(listResponse);
      setHandleLoading(false);
    } catch (error) {
      if (error.status === 401) {
        toast.error('Usuario expirado, logue novamente!');
        sessionStorage.clear();
        history.push('/login');
      }
      setHandleLoading(false);
      setList([]);
    }
  };

  return (
    <div className='Home'>
      {!window.sessionStorage.getItem('accessToken') ? (
        <Redirect
          to={{
            pathname: '/login',
            state: { from: location },
          }}
        />
      ) : (
        ''
      )}
      <ToastContainer />
      <Header
        backBtn={false}
        title={''}
        searchIcon={true}
        changeSearchInput={handleChangeSearchbarInput}
      />
      <main>
        {list.length === 0 ? (
          <div className='without-data'>
            {handleLoading ? (
              <div className='loading'>
                <Loader type='Rings' color='#00BFFF' height={80} width={80} />
              </div>
            ) : (
              <div className='text'>
                {input
                  ? 'Nenhuma empresa foi encontrada para a busca realizada.'
                  : 'Clique na busca para iniciar.'}
              </div>
            )}
          </div>
        ) : (
          <div className='list'>
            {list.map((el) => {
              return (
                <Card
                  key={el.id}
                  id={el.id}
                  title={el.enterprise_name}
                  img={'https://empresas.ioasys.com.br' + el.photo}
                  category={el.enterprise_type.enterprise_type_name}
                  country={el.country}
                />
              );
            })}
          </div>
        )}
      </main>
    </div>
  );
};
