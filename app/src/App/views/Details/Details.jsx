import React, { useEffect, useState } from 'react';
import { useParams, useHistory, Redirect, useLocation } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.min.css';
import { toast, ToastContainer } from 'react-toastify';

import './Details.css';

import { getById } from '../../providers/enterprises';

import Header from './../../components/header/Header';
import BigCard from './../../components/BigCard/BigCard';

export default (_) => {
  const { id } = useParams();
  const [enterprise, setEnterprise] = useState({ enterprise_name: 'Company' });
  const history = useHistory();
  const location = useLocation();

  const getEnterprise = () => {
    getById(id)
      .then((response) => {
        console.log(response);

        setEnterprise(response);
      })
      .catch((error) => {
        if (error.status === 401) {
          toast.error('Usuario expirado, logue novamente!');
          sessionStorage.clear();
          history.push('/login');
        }
        toast.error('Empresa invalida!');
        history.push('/');
      });
  };

  useEffect(() => {
    getEnterprise();
  }, [setEnterprise]);

  return (
    <div className='Details'>
      {!window.sessionStorage.getItem('accessToken') ? (
        <Redirect
          to={{
            pathname: '/login',
          }}
        />
      ) : (
        ''
      )}
      <Header
        backBtn={true}
        title={enterprise.enterprise_name}
        searchIcon={false}
        changeSearchInput={null}
      />
      <main>
        <BigCard
          id={enterprise.id}
          img={'https://empresas.ioasys.com.br' + enterprise.photo}
          description={enterprise.description}
        />
      </main>
    </div>
  );
};
