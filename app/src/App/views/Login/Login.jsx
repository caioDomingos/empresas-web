import React, { useState } from 'react';
import { useHistory, useLocation, Redirect } from 'react-router-dom';
import './Login.css';

import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faLock } from '@fortawesome/free-solid-svg-icons';
import { faEye } from '@fortawesome/free-solid-svg-icons';
import { faEyeSlash } from '@fortawesome/free-solid-svg-icons';

import { signIn } from './../../providers/auth';

export default (_) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordField, setPasswordField] = useState('password');

  const history = useHistory();
  const location = useLocation();

  const onSubmit = async () => {
    try {
      console.log('email', email);
      console.log('password', password);

      await signIn({ email, password });
      history.push('/');
      toast.success('Logado com sucesso');
    } catch (error) {
      console.error('Error on onSubmit', error);

      if (error.status === 401) {
        toast.error('Erro de login');
      } else {
        toast.error('Erro interno');
      }
    }
  };

  const changePasswordFieldType = () => {
    passwordField === 'password'
      ? setPasswordField('text')
      : setPasswordField('password');
  };

  return (
    <main className='Login'>
      <ToastContainer />
      {/* {window.sessionStorage.getItem('accessToken') ? (
        <Redirect
          to={{
            pathname: '/',
            state: { from: location },
          }}
        />
      ) : (
        ''
      )} */}

      <div className='modal'>
        <img
          src='img/logo-home.png'
          srcSet='img/logo-home@2x.png 2x,
                   img/logo-home@3x.png 3x'
          className='logo_home'
        ></img>
        <p className='title'>BEM-VINDO AO EMPRESAS</p>
        <p className='subtitle'>
          Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
        </p>
        <div className='content-input'>
          <span className='icon start'>
            <FontAwesomeIcon icon={faEnvelope} />
          </span>
          <input
            className='input'
            type='email'
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className='content-input'>
          <span className='icon start'>
            <FontAwesomeIcon icon={faLock} />
          </span>
          <input
            className='input'
            type={passwordField}
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <span className='icon end' onClick={(_) => changePasswordFieldType()}>
            <FontAwesomeIcon
              style={
                passwordField === 'password'
                  ? { display: 'inline' }
                  : { display: 'none' }
              }
              icon={faEye}
            />
            <FontAwesomeIcon
              style={
                passwordField === 'password'
                  ? { display: 'none' }
                  : { display: 'inline' }
              }
              icon={faEyeSlash}
            />
          </span>
        </div>

        <button className='button' onClick={(_) => onSubmit()}>
          Entrar
        </button>
      </div>
    </main>
  );
};
