import { httpRequest } from './utils';

export const getByName = async (queryParams) => {
  try {
    const axiosConfig = {
      headers: {
        ['access-token']: window.sessionStorage.getItem('accessToken'),
        ['client']: window.sessionStorage.getItem('client'),
        ['uid']: window.sessionStorage.getItem('uid'),
      },
    };

    const config = {
      queryParams: '?name=' + queryParams,
      method: 'GET',
      axiosConfig,
    };

    console.log(config);
    const response = await httpRequest(
      'https://empresas.ioasys.com.br/api/v1/enterprises',
      config
    );

    return response.data.enterprises;
  } catch (error) {
    console.error('Error on getByName', error);
    throw error;
  }
};


export const getById = async (queryParams) => {
  try {
    const axiosConfig = {
      headers: {
        ['access-token']: window.sessionStorage.getItem('accessToken'),
        ['client']: window.sessionStorage.getItem('client'),
        ['uid']: window.sessionStorage.getItem('uid'),
      },
    };

    const config = {
      queryParams: '/' + queryParams,
      method: 'GET',
      axiosConfig,
    };

    console.log(config);
    const response = await httpRequest(
      'https://empresas.ioasys.com.br/api/v1/enterprises',
      config
    );

    return response.data.enterprise;
  } catch (error) {
    console.error('Error on getById', error);
    throw error;
  }
};
