import { httpRequest } from './utils';

export const signIn = async (body) => {
  try {
    const config = {
      body,
      method: 'POST',
    };

    const response = await httpRequest(
      'https://empresas.ioasys.com.br/api/v1/users/auth/sign_in',
      config
    );

    console.log('response', response);

    const accessToken = response.headers['access-token'];
    const client = response.headers.client;
    const uid = response.headers.uid;

    console.log('headers', [accessToken, client, uid]);

    window.sessionStorage.setItem('accessToken', accessToken);
    window.sessionStorage.setItem('client', client);
    window.sessionStorage.setItem('uid', uid);

    return response;
  } catch (error) {
    console.error('Error on signIn', error);
    throw error;
  }
};
