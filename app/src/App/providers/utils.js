import axios from 'axios';

export const httpRequest = async (url, config) => {
  try {
    console.log('Requesting...', url);
    console.log('Requesting...', config);
    const cancelToken = axios.CancelToken;
    const source = cancelToken.source();
    const axiosConfig = config.axiosConfig
      ? { ...config.axiosConfig, cancelToken: source.token }
      : { cancelToken: source.token };

    const body = config.body ? config.body : {};
    const queryParams = config.queryParams ? config.queryParams : '';

    const requestUrl = `${url}${queryParams ? queryParams : ''}`;
    console.log(axiosConfig);
    console.log(requestUrl);

    const response = await (config.method === 'POST'
      ? axios.post(requestUrl, body, axiosConfig)
      : axios.get(requestUrl, axiosConfig));

    return response;
  } catch (error) {
    console.error('error httpRequest', error.response);
    throw error.response;
  }
};
