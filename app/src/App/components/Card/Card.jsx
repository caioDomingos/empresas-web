import React from 'react';
import './Card.css';

import { useHistory } from 'react-router-dom';

export default (props) => {
  const id = props.id;
  const title = props.title;
  const img = props.img;
  const category = props.category;
  const country = props.country;

  const history = useHistory();
  const goToDetails = () => {
    history.push(`/details/${id}`);
  };

  return (
    <div className='Card' onClick={(_) => goToDetails()}>
      <div className='img-content'>
        <img src={img} />
      </div>
      <div className='text-content'>
        <span className='title'>{title}</span>
        <span className='category'>{category}</span>
        <span className='country'>{country}</span>
      </div>
    </div>
  );
};
