import React from 'react';
import './BigCard.css';

import { useHistory } from 'react-router-dom';

export default (props) => {
  const id = props.id;
  const img = props.img;
  const description = props.description;

  const history = useHistory();
  const goToDetails = () => {
    history.push(`/details/${id}`);
  };

  return (
    <div className='BigCard' onClick={(_) => goToDetails()}>
      <div className='img-content'>
        <img src={img} />
      </div>
      <p className='text'>{description}</p>
    </div>
  );
};
