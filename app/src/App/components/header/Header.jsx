import React, { useEffect, useState } from 'react';
import './Header.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

import { useHistory } from 'react-router-dom';

export default (props) => {
  const backBtn = props.backBtn;
  const [searchIcon, setSearchIcon] = useState(props.searchIcon);
  const [showSearchInput, setShowSearchInput] = useState(false);

  const title = props.title;
  const [searchInput, setSearchInput] = useState('');

  const [handleChangeTimeOutId, setHandleChangeTimeOutId] = useState(null);

  const history = useHistory();

  const goBack = () => history.goBack();

  useEffect(() => {
    if (!props.changeSearchInput) {
      return;
    }

    if (handleChangeTimeOutId) {
      console.log('here');
      clearTimeout(handleChangeTimeOutId);
    } else {
      console.log('else', handleChangeTimeOutId);
    }

    const id = setTimeout(() => {
      props.changeSearchInput(searchInput);
    }, 500);

    setHandleChangeTimeOutId(id);
  }, [searchInput]);

  const handleSearchbarChange = () => {
    if (!searchIcon) {
      setSearchInput('');
    }
    setSearchIcon(!searchIcon);
    setShowSearchInput(!showSearchInput);
  };

  return (
    <div className='Header'>
      {backBtn && !showSearchInput ? (
        <span className='backBtn' onClick={(_) => goBack()}>
          <FontAwesomeIcon icon={faArrowLeft} />
        </span>
      ) : (
        ''
      )}
      {!showSearchInput ? (
        title ? (
          <span>
            <p className='title'>{title}</p>
          </span>
        ) : (
          <img
            src='img/logo-nav.png'
            srcSet='img/logo-nav@2x.png 2x,
             img/logo-nav@3x.png 3x'
          />
        )
      ) : (
        ''
      )}
      {showSearchInput ? (
        <div className='input-content'>
          <FontAwesomeIcon className='searchbar-icon search' icon={faSearch} />
          <input
            placeholder='Pesquisar'
            type='text'
            value={searchInput}
            onChange={(e) => setSearchInput(e.target.value)}
          />
          <FontAwesomeIcon
            className='searchbar-icon'
            onClick={(_) => handleSearchbarChange()}
            icon={faTimes}
          />
        </div>
      ) : (
        ''
      )}
      {searchIcon ? (
        <FontAwesomeIcon
          className='icon'
          onClick={(_) => handleSearchbarChange()}
          icon={faSearch}
        />
      ) : (
        ''
      )}
    </div>
  );
};
