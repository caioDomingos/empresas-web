import React from 'react';
import ReactDOM from 'react-dom';

import './global.css';
import './reset.css';

import App from './App/App';

ReactDOM.render(<App />, document.getElementById('root'));
